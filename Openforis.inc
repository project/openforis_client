<?php

class Openforis {
  private $error = null;
  private $domain = null;
  private $apikey = null;
  private $appcode = null;
  private $address = null;
  private $helper = null;
  
  function __construct($domain, $apikey, $appcode, $address, $helper_class) {
      $this->domain = $domain;
      $this->apikey = $apikey;
      $this->appcode = $appcode;
      $this->address = $address;
      
      require_once($helper_class . '.inc');
      $this->helper = new $helper_class($this);
  }
  
  public function ping() {
    $ret = $this->xmlrpc('openforis.ping');
    if (isset($ret['code'])) {
      return $ret['code'];
    }
    return 0;
  }
  
  public function information() {
    $ret = $this->xmlrpc('openforis.system.information');
    return $ret;
  }

  public function authenticate($user, $pass) {
    $ret = $this->xmlrpc('openforis.user.login', array($user, $pass));
    return $ret;
  }
  
  public function add($name, $mail, $pass, $values) {
    $ret = $this->xmlrpc('openforis.user.create', array($name, $mail, $pass, $values));
    return $ret;
  }
  
  public function connect($name, $mail, $uid) {
    $ret = $this->xmlrpc('openforis.user.connect', array($name, $mail, $uid));
    return $ret;
  }
  
  public function update($mail, $values) {
    $ret = $this->xmlrpc('openforis.user.update', array($mail, $values));
    return $ret;
  }
  
  public function delete($mail) {
    $ret = $this->xmlrpc('openforis.user.delete', array($mail));
    return $ret;
  }
  
  public function accounts_get($mail) {
    $ret = $this->xmlrpc('openforis.user.get_accounts', array($mail));
    return $ret;
  }
  
  public function search($mode, $value) {
    $ret = $this->xmlrpc('openforis.user.search', array($mode, $value));
    return $ret;
  }
  
  public function exclusion_set($names, $mails) {
    $ret = $this->xmlrpc('openforis.exclusion.update', array($names, $mails));
    return $ret;
  } 

  public function fields_get() {
    $ret = $this->xmlrpc('openforis.profile.get_fields');
    return $ret;
  } 

  public function queue_get() {
    $ret = $this->xmlrpc('openforis.queue.get');
    return $ret;
  } 

  public function queue_delete($qid) {
    $ret = $this->xmlrpc('openforis.queue.delete', array($qid));
    return $ret;
  } 
  
  public function xmlrpc($method, $params = array()) {
    $this->error = null;
    
    $uri = parse_url($this->address);
    $port = isset($uri['port']) ? ':'. $uri['port'] : '';
    $path = isset($uri['path']) ? $uri['path'] : '';
    $endpoint = $uri['host'] . $port . $path;
    $scheme = $uri['scheme'];

    $endpoint = $scheme . '://'. $endpoint;
    $auth = $this->authenticator($method);
    $auth[] = $this->appcode;

    $params = array_merge($auth, $params);
    
    return $this->helper->xmlrpc($endpoint, $method, $params);
  }
  
  public function authenticator($method) {
    $timestamp = (string) time();
    $nonce = $this->_generate_nonce();
    $hash = hash_hmac('sha256', $timestamp .';'. $this->domain .';'. $nonce .';'. $method, $this->apikey);

    $auth[] = $hash;
    $auth[] = $this->domain;
    $auth[] = $timestamp;
    $auth[] = $nonce;
    return $auth;
  }
  
  public function error_log($code, $message) {
    $this->error = $this->helper->error_log($code, $message);
  }
  
  public function error_get() {
    return $this->error;
  }
  
  private function _generate_nonce() {
    $allowable_characters = 'abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789';
    $len = strlen($allowable_characters) - 1;
    $nonce = '';

    for ($i = 0; $i < 10; $i++) {
      $nonce .= $allowable_characters[mt_rand(0, $len)];
    }

    return $nonce;
  }
}