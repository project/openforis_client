<?php

class OpenforisDrupal {
  private $parent;
   
  function __construct($parent) {
    $this->parent = $parent;
  }
  
  function xmlrpc($endpoint, $method, $params) {
    $params = array_merge(array($endpoint, $method), $params);

    $data = call_user_func_array('xmlrpc', $params);
    if ($error = xmlrpc_error()) {
      $this->parent->error_log($error->code, $error->message);
      return FALSE;
    }
    if ($data['code'] == 10) {
      $this->parent->error_log(10, 'The application code is incorrect.');
      return FALSE;
    }
    
    return $data;
  }
  
  function error_log($code, $message) {
    watchdog('openforis', 'Openforis error: @code - "@msg"', array('@msg' => $message, '@code' => $code), WATCHDOG_ERROR);
    return t('Openforis error: @code - "@msg"', array('@msg' => $message, '@code' => $code));
  }
}