<?php

/**
 * Provides the settings form.
 *
 */
function openforis_client_settings() {
  $form = array();
  
  $form['openforis_client_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable the distributed authentication'),
    '#default_value' => variable_get('openforis_client_enabled', FALSE),
    '#description' => t('Enabling this makes Drupal use the login server.'),
  );

  $network = variable_get('openforis_network_name', '');
  if ($network) {
    $form['openforis_server_information'] = array(
      '#type' => 'markup',
      '#value' => theme('openforis_client_system_information'),
    );
  }

  $form['openforis_client_queue_process'] = array(
    '#type' => 'radios',
    '#title' => t('When to process new data'),
    '#options' => array(
      0 => t('When notified by the server and on cron'),
      1 => t('Just on cron'),
    ),
    '#default_value' => variable_get('openforis_client_queue_process', 0),
  );  

  $form['openforis_client_num_retries'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of retries'),
    '#default_value' => variable_get('openforis_client_num_retries', 5),
    '#description' => t('The number of times the system will retry to update information on the server.') . (module_exists('job_queue') ? '' : ' <strong>'. t('Retrying requires the <a href="http://drupal.org/project/job_queue">Job Queue</a> module.') .'</strong>'),
    '#disabled' => module_exists('job_queue') ? FALSE : TRUE,
  );
    
  if (module_exists('content_profile')) {
    $coptions = array();
    foreach (content_profile_get_types('names') as $type => $type_name) {
      $coptions[$type] = $type_name;
    }
    if (!count($coptions)) {
      drupal_set_message(t('To automatically create a profile for users who connect to this website you need to have at least one content type marked as a user profile.'));
    }
    else {
      $form['openforis_client_profile_type'] = array(
        '#type' => 'select',
        '#title' => t('Create a profile using'),
        '#options' => $coptions,
        '#default_value' => variable_get('openforis_client_profile_type', ''),
      );
    }
  }
  
  $form['api_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Api access'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['api_info']['openforis_client_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Openforis server'),
    '#default_value' => variable_get('openforis_client_server', ''),
    '#description' => t('Address of the Openforis server.'),
  );

  $form['api_info']['openforis_client_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Api key'),
    '#default_value' => variable_get('openforis_client_api_key', ''),
    '#description' => t('Api key to use.'),
  );

  $form['api_info']['openforis_client_appcode'] = array(
    '#type' => 'textfield',
    '#title' => t('Application code'),
    '#default_value' => variable_get('openforis_client_appcode', ''),
    '#description' => t('The application code. Must match the one on the login server.'),
  );
  
  $form['api_info']['openforis_client_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Address of this website'),
    '#default_value' => variable_get('openforis_client_domain', drupal_valid_http_host($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ''),
    '#description' => t('The address of this website. Will be used by xmlrpc.'),
  );

  $form['#submit'][] = 'openforis_client_settings_submit';

  return system_settings_form($form);
}

/**
 * Custom submit handler which gets data from the server.
 *
 */
function openforis_client_settings_submit($form, $form_state) {
  $domain = $form_state['values']['openforis_client_domain'];
  $apikey = $form_state['values']['openforis_client_api_key'];
  $appcode = $form_state['values']['openforis_client_appcode'];
  $address = $form_state['values']['openforis_client_server'];
  
  if (!empty($domain) && !empty($apikey) && !empty($appcode) && !empty($address)) {
    if (!_openforis_client_get_information($domain, $apikey, $appcode, $address)) {
      $openforis = openforis_client_get_openforis();
      $error = $openforis->error_get();
      
      drupal_set_message(t('Failed to get information about the login server. Error: @error.', array('@error' => $error)), 'error');
    }
    else {
      drupal_set_message(t('Sucessfully contacted the login server.'));
    }
  }
}

/**
 * Provides the exclusion form.
 *
 */
function openforis_client_exclusion() {
  $form = array();

  $names = variable_get('openforis_client_exclusion_names', array());
  $mails = variable_get('openforis_client_exclusion_mails', array());
  

  $form['openforis_client_exclusion_names'] = array(
    '#type' => 'textarea',
    '#title' => t('Usernames'),
    '#default_value' => $names ? implode("\n", $names) : '',
    '#description' => t('Usernames to be excluded. Enter one per line.'),
    );

  $form['openforis_client_exclusion_mails'] = array(
    '#type' => 'textarea',
    '#title' => t('E-mails'),
    '#default_value' => $mails ? implode("\n", $mails) : '',
    '#description' => t('E-mail addresses to be excluded. Enter one per line.'),
    );

  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Save'),
  );
  
  return $form;
}

function openforis_client_exclusion_validate($form, &$form_state) {
  $names = $form_state['values']['openforis_client_exclusion_names'];
  $mails = $form_state['values']['openforis_client_exclusion_mails'];
  
  $names = split("\n", $names);
  $mails = split("\n", $mails);
  
  foreach ($mails as $mail) {
    $mail = trim($mail);
    if (empty($mail)) {
      continue;
    }
    if (!valid_email_address($mail)) {
      form_set_error('openforis_client_exclusion_mails', t('Some of the emails entered are not valid.'));
    }
  }
}

function openforis_client_exclusion_submit($form, &$form_state) {
  $names = $form_state['values']['openforis_client_exclusion_names'];
  $mails = $form_state['values']['openforis_client_exclusion_mails'];
  
  $names = split("\n", $names);
  $mails = split("\n", $mails);
  
  // cleanup
  $snames = array();
  $smails = array();
  foreach ($names as $name) {
    $name = trim($name);
    if (!empty($name)) {
      $snames[] = $name;
    }
  }
  foreach ($mails as $mail) {
    $mail = trim($mail);
    if (!empty($mail)) {
      $smails[] = trim($mail);
    }
  }
  
  variable_set('openforis_client_exclusion_names', $snames);
  variable_set('openforis_client_exclusion_mails', $smails);
  
  $openforis = openforis_client_get_openforis();
  $result = $openforis->exclusion_set($snames, $smails);
  if ($result->code == 0) {
    drupal_set_message(t('The values were saved.'));
  }
  else {
    drupal_set_message(t('There was an error saving the values on the server.'));
  }
}

function openforis_client_migrate() {
  $form = array();
  
  drupal_add_js(drupal_get_path('module', 'openforis_client') .'/openforis_client.js');
  
  $form['cmigrate_all'] = array(
    '#type' => 'checkbox',
  );

  $result = db_query('SELECT * FROM {users} WHERE remote = 0 AND uid NOT IN (0, 1)');
  while ($row = db_fetch_object($result)) {
    $form['checkboxes']['migrate_'. $row->uid] = array(
      '#type' => 'checkbox',
      '#user_uid' => $row->uid,
      '#user_name' => $row->name,
      '#user_mail' => $row->mail,
    );
  }
  
  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Migrate'),
  );
  return $form;
}

function openforis_client_migrate_submit($form, &$form_state) {
  $openforis = openforis_client_get_openforis();
  
  foreach ($form_state['values'] as $name => $val) {
    if (strpos($name, 'migrate_') === 0) {
      if ($val) {
        $uids[] = str_replace('migrate_', '', $name);
      }
    }
  }
  
  if (count($uids)) {
    foreach ($uids as $uid) {
      $user = user_load(array('uid' => $uid));
      // is the username taken?
      $oname = $user->name;
      $name = $user->name;
      $result = $openforis->search('name', $user->name);
      $naccount = $result['data'];
      while ($result && $result['code'] == 0) {
        $name = $user->name . user_password();
        $result = $openforis->search('name', $name);
      } 
      
      // do we have a user with this email address?
      $result = $openforis->search('mail', $user->mail);
      if ($result && $result['code'] == 0) {
        $eaccount = $result['data'];
        if ($eaccount['uid'] != $naccount['uid']) {
          $user->name = $name;
        }
        // we'll connect the local user to the remote user then
        $edit = array();
        $edit['name'] = $user->name;
        $edit['uid'] = $user->uid;
        $response = $openforis->update($user->mail, $edit);
        if ($response && $response['code'] == 0) {
          user_save($user, array('name' => $user->name));
          db_query('UPDATE {users} SET remote = 1 WHERE uid = %d', $user->uid);
          if ($oname == $user->name) {
            drupal_set_message(t('Connected user %user to the user already on the server.', array('%user' => $user->name)));
          }
          else {
            drupal_set_message(t('Connected user %user to the user already on the server. The new username is: %nuser', array('%user' => $oname, '%nuser' => $user->name)));
          }
        }
        else {
          drupal_set_message(t('Failed to connect user %user to the server.', array('%user' => $oname)));
        }
      }
      else {
        $user->name = $name;
        $pass = db_result(db_query('SELECT pass FROM {users} WHERE uid = %d', $user->uid));
        $edit['name'] = $user->name;
        $edit['mail'] = $user->mail;
        $edit['uid'] = $user->uid;
        $edit['epass'] = $pass;
        $edit['status'] = $user->status;
        $result = $openforis->add($user->name, $user->mail, $user->pass, $edit);
        if ($result && $result['code'] == 0) {
          user_save($user, array('name' => $user->name));
          db_query('UPDATE {users} SET remote = 1 WHERE uid = %d', $user->uid);
          if ($oname == $user->name) {
            drupal_set_message(t('Connected user %user to the server.', array('%user' => $user->name)));
          }
          else {
            drupal_set_message(t('Connected user %user to the server. The new username is: %nuser', array('%user' => $oname, '%nuser' => $user->name)));
          }
        }
        else {
          drupal_set_message(t('Failed to connect user %user to the server.', array('%user' => $oname)));
        }
      }
    }
  }
}


function openforis_client_field_mapping() {
  if (!module_exists('content_profile')) {
    drupal_set_message(t('Field mapping requires the <a href="http://drupal.org/project/content_profile">"Content Profile"</a> module to be installed and at least one content type to be marked as a user profile.'));
    drupal_goto('admin/user/openforis');
  }
  
  $domain = variable_get('openforis_client_domain', drupal_valid_http_host($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '');
  if (empty($domain)) {
    drupal_set_message(t('Please enter the server settings.'));
    drupal_goto('admin/user/openforis');
  }
  $openforis = openforis_client_get_openforis();
  
  $profiles = content_profile_get_types();
  $num = count($profiles);
  if ($num == 0) {
    drupal_set_message(t('Field mapping requires at least one content type to be marked as a user profile.'));
    drupal_goto('admin/user/openforis');
  }

  $lfields = array();

  foreach ($profiles as $p) {
    $content = content_types($p->type);
    foreach ($content['fields'] as $name => $value) {
      $lfields[] = array('content' => $p->type, 'content_display' => $p->name, 'name' => $name, 'display' => $value['widget']['label'], 'type' => $value['type']);
    }
  }
  
  if (empty($lfields)) {
    drupal_set_message(t('The profile content types have no fields defined.'));
    drupal_goto('admin/user/openforis');
  }
  
  $result = $openforis->fields_get();
  if ($result && $result['code'] == 0) {
    $fields = $result['data'];
  }
  else {
    drupal_set_message(t('Can\'t query the server for fields.'));
    drupal_goto('admin/user/openforis');
  }

  if (!count($fields)) {
    $form['fields'] = array(
      '#value' => t('There are no fields available for this site on the server.'),
    );
  }
  else {
    $mapping = variable_get('openforis_client_field_mapping', array());
    $noption = array(0 => t('Nothing'));
    foreach ($fields as $field) {
      $rfields[$field['type']][$field['name']] = $field['label'] .($field['description'] ? ' - '. $field['description'] : '');
    }
    
    $form['mapping'] = array(
      '#tree' => TRUE,
    );

    foreach ($lfields as $field) {
      $form['fields'][$field['content'] . $field['name']] = array(
        '#value' => '',
        '#field' => $field,
      );
      $form['mapping'][$field['content']][$field['name']] = array(
        '#type' => 'select',
        '#options' => array_merge($noption, isset($rfields[$field['type']]) ? $rfields[$field['type']] : array()),
        '#default_value' => isset($mapping[$field['content']][$field['name']]) ? $mapping[$field['content']][$field['name']] : 0,
      );
    }
  }
  
  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Save mapping'),
  );
  
  return $form;
}

function openforis_client_field_mapping_submit($form, &$form_state) {
  $profiles = content_profile_get_types();
  foreach ($form_state['values']['mapping'] as $profile => $mapping) {
    foreach ($mapping as $field => $fid) {
      if ($fid) {
        $map[$profile][$field] = $fid;
      }
    }
  }
  
  variable_set('openforis_client_field_mapping', $map);
}