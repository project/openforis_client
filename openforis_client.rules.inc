<?php

/**
* Implementation of hook_rules_event_info().
* @ingroup rules
*/
function openforis_client_rules_event_info() {
  return array(
    'openforis_client_user_add' => array(
      'label' => t('User connected to the site.'),
      'module' => 'Openforis Client',
      'arguments' => array(
        'user' => array('type' => 'user', 'label' => t('The user who connected to the site.')),
      ),
    ),
    'openforis_client_user_update' => array(
      'label' => t('User was updated.'),
      'module' => 'Openforis Client',
      'arguments' => array(
        'user' => array('type' => 'user', 'label' => t('The user who was updated.')),
      ),
    ),
    'openforis_client_user_delete' => array(
      'label' => t('User disconnected from the login server.'),
      'module' => 'Openforis Client',
      'arguments' => array(
        'user' => array('type' => 'user', 'label' => t('The user who disconnected.')),
      ),
    ),
  );
}