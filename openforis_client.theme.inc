<?php

function theme_openforis_client_field_mapping($form) {
  $header = array(t('Content type'), t('Field name'), t('Field'), t('Type'), t('Sync with remote field'));

  foreach ($form['fields'] as $key => $element) {
    if (isset($element['#field'])) {
      $rows[] = array($element['#field']['content_display'], $element['#field']['display'], $element['#field']['name'], $element['#field']['type'], drupal_render($form['mapping'][$element['#field']['content']][$element['#field']['name']]));
    }
  }
  
  if (count($rows)) {
    $output .= theme('table', $header, $rows);
    $output .= drupal_render($form);
  }

  return $output;
}

function theme_openforis_client_migrate($form) {
  $header = array(t('User id'), t('Name'), t('Mail'), drupal_render($form['cmigrate_all']));
  
  if (count($form['checkboxes'])) {
    foreach ($form['checkboxes'] as $key => $element) {
      if (isset($element['#user_uid'])) {
        $rows[] = array($element['#user_uid'], $element['#user_name'], $element['#user_mail'], drupal_render($form['checkboxes'][$key]));
      }
    }
  }

  if (count($rows)) {
    $output .= theme('table', $header, $rows);
    $output .= drupal_render($form);
  }
  else {
    $output .= theme('placeholder', t('There are no users who can be migrated.'));
    $output .= '<br />';
  }
  return $output;
}

function theme_openforis_client_system_information() {
  $output = '';
  
  $output .= '<p><strong>'. t('Server information') .'</strong></p>';
  $output .= '<ul>';
  $output .= '<li>'. t('Network name') .': '. variable_get('openforis_network_name', '') .'</li>';
  $output .= '<li>'. t('Use keys') .': '. (variable_get('openforis_use_key', FALSE) ? t('Yes') : t('No')) .'</li>';
  $output .= '<li>'. t('Use session id') .': '. (variable_get('openforis_use_sessid', FALSE) ? t('Yes') : t('No')) .'</li>';
  $output .= '</ul>';
  
  return $output;
}